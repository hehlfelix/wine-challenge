const express = require('express');
const router = express.Router();
const wineBaseController = require('../controllers/winebase.controller');

/* GET wine */
router.get('/', wineBaseController.get);

/* POST wine */
router.post('/', wineBaseController.create);

/* PUT wine */
router.put('/', wineBaseController.update);

/* DELETE wine by ID*/
router.delete('/:id', wineBaseController.remove);

/* GET wine by ID*/
router.get('/:id', wineBaseController.getByID);

module.exports = router;
