const dbConfig = require('../configs/db.config');
const Pool = require('pg').Pool;
const pool = new Pool(dbConfig);

/** Database query function
 * @param {any} sql - sql statement for query
 * @param {any} params - parameters for query
 * */
async function query(sql, params) {
  let response;
  try {
    response = await pool.query(sql, params);
    return response.rows;
  } catch (error) {
    return -1;
  }
}

module.exports = {
  query,
};
