const db = require('./db.service');
const helper = require('../utils/helper.util');
const validation = require('../utils/validation.util');

/** Query - all wines from wine database */
async function getMultiple() {
  const rows = await db.query(`SELECT w.id, w.name as name, 
            w.country, t.name as type, w.description, 
            w.price FROM api.wines w, api.types t where w.type = t.id`);
  return helper.emptyOrRows(rows);
}

/** Query - wine where id is request param id
 *  @param {any} id - id for finding specific wine, will be validated
 * */
async function getByID(id) {
  let message = 'No such wine';
  const valID = validation.validateID(id);
  if (!valID[0]) {
    message = valID[1];
    return {message};
  }
  const result = await db.query(`SELECT w.id, w.name as name, w.country, 
            t.name as type, w.description, w.price
            FROM api.wines w , api.types t
            WHERE w.type = t.id AND w.id = $1`, [id]);
  if (result !== -1) {
    return result[0];
  }
  return {message};
}

/** Query - insert wine by given request body after validation
 * @param {any} wine - wine representation for creating wine, will be validated
 * */
async function create(wine) {
  let message = 'Error in creating wine';
  const valWine = validation.validateWineWithoutID(wine);
  if (!valWine[0]) {
    message = valWine[1];
    return {message};
  }

  const result = await db.query(`INSERT INTO api.wines 
              (name, year, country, type, description, price) 
              VALUES ($1, $2, $3, (SELECT id FROM api.types 
              WHERE name = $4), $5, $6) RETURNING id`,
  [wine.name, wine.year, wine.country, wine.type,
    wine.description, wine.price]);
  if (result !== -1) {
    wine.id = result[0].id;
    return wine;
  }
  return {message};
}

/** Query - updates wine by given request body after validation
 * @param {any} wine - wine representation for updating specific wine,
 *                    will be validated
 * */
async function update(wine) {
  let message = 'Error in updating wine';
  const valWine = validation.validateWineWithID(wine);
  if (!valWine[0]) {
    message = valWine[1];
    return {message};
  }
  const result = await db.query(`UPDATE api.wines SET name = $1, 
                year = $2, country = $3, 
                type = (SELECT id FROM api.types WHERE name = $4), 
                description = $5, price = $6 WHERE id = $7`,
  [wine.name, wine.year, wine.country, wine.type,
    wine.description, wine.price, wine.id]);
  if (result !== -1) {
    return wine;
  }
  return {message};
}

/** Query - removes where id is given request param id
 * @param {any} id - id for finding specific wine, will be validated
 * */
async function remove(id) {
  let message = 'Error in deleting wine';
  const valID = validation.validateID(id);
  if (!valID[0]) {
    message = valID[1];
    return {message};
  }
  const result = await db.query(`DELETE FROM api.wines WHERE id = $1`, [id]);
  if (result !== -1) {
    message = `Deleted wine with ID ${id}`;
  }
  return {message};
}

module.exports = {
  getMultiple, getByID, create, update, remove,
};
