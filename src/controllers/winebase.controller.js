const wineBase = require('../services/winebase.service');

/** Operation: getWines
 * @param {any} req - request
 * @param {any} res - response
 * @param {any} next - invokes next mw or route handler
 * */
async function get(req, res, next) {
  try {
    res.json(await wineBase.getMultiple());
  } catch (err) {
    console.error(`Error while getting wines`, err.message);
    next(err);
  }
}
/** Operation: getByID
 * @param {any} req - request
 * @param {any} res - response
 * @param {any} next - invokes next mw or route handler
 * */
async function getByID(req, res, next) {
  try {
    res.json(await wineBase.getByID(req.params.id));
  } catch (err) {
    console.error(`Error while getting wines`, err.message);
    next(err);
  }
}
/** Operation: createWine
 * @param {any} req - request
 * @param {any} res - response
 * @param {any} next - invokes next mw or route handler
 * */
async function create(req, res, next) {
  try {
    res.json(await wineBase.create(req.body));
  } catch (err) {
    console.error(`Error while creating wine`, err.message);
    next(err);
  }
}
/** Operation: updateWine
 * @param {any} req - request
 * @param {any} res - response
 * @param {any} next - invokes next mw or route handler
 * */
async function update(req, res, next) {
  try {
    res.json(await wineBase.update(req.body));
  } catch (err) {
    console.error(`Error while updating wine`, err.message);
    next(err);
  }
}

/** Operation: deleteWineByID
 * @param {any} req - request
 * @param {any} res - response
 * @param {any} next - invokes next mw or route handler
 * */
async function remove(req, res, next) {
  try {
    res.json(await wineBase.remove(req.params.id));
  } catch (err) {
    console.error(`Error while deleting wine`, err.message);
    next(err);
  }
}

module.exports = {
  get,
  getByID,
  create,
  update,
  remove,
};
