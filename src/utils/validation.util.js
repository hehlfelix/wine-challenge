const Ajv = require('ajv');
const wineTypes = ['red', 'white', 'rose'];
const wineSchema = {
  type: 'object', properties: {
    name: {type: 'string'},
    year: {type: 'number'},
    country: {type: 'string'},
    type: {enum: wineTypes},
    description: {type: 'string'},
    price: {type: 'number'},
  }, required: ['name', 'year', 'country', 'type'], additionalProperties: false,
};

const wineSchemaWithID = {
  type: 'object', properties: {
    id: {type: 'number'},
    name: {type: 'string'},
    year: {type: 'number'},
    country: {type: 'string'},
    type: {enum: wineTypes},
    description: {type: 'string'},
    price: {type: 'number'},
  }, required: ['id', 'name', 'year', 'country', 'type'],
  additionalProperties: false,
};

const validator = new Ajv({allErrors: true});

/** Validates wine by wineSchemaWithID
 * @param {any} wine - wine representation
 * @return {array} validation result - contains boolean and msg
 * */
function validateWineWithID(wine) {
  return validateWine(wine, wineSchemaWithID);
}
/** Validates wine by wineSchemaWithoutID
 * @param {any} wine - wine representation
 * @return {array} validation result - contains boolean and msg
 * */
function validateWineWithoutID(wine) {
  return validateWine(wine, wineSchema);
}

/** Validates wine by schema with or without ID
 * @param {any} wine - wine representation
 * @param {any} schema - wine schema to validate against
 * @return {array} validation result - contains boolean and msg
 * */
function validateWine(wine, schema) {
  const validWine = validator.validate(schema, wine);
  if (validWine) {
    return [true, ''];
  }
  const errorMsg = [];
  for (const e in validator.errors) {
    if (Object.prototype.hasOwnProperty.call(validator.errors, e)) {
      const desc = {
        'error on field': validator.errors[e[0]]['instancePath'],
        'error': validator.errors[e[0]]['message'],
      };
      if (desc['error on field'] === '/type') {
        desc['error'] = validator.errors[e[0]]['message']
            .concat(' ', wineTypes.toString().replaceAll(',', ', '));
      }
      if (validator.errors[e[0]]['keyword'] === 'required') {
        desc['error on field'] = validator
            .errors[e[0]]['params']['missingProperty'];
      }
      errorMsg.push(desc);
    }
  }
  return [false, errorMsg];
}

/** Validates given ID
 * @param {any} id - id
 * @return {array} validation result - contains boolean and msg
 * */
function validateID(id) {
  if (isNaN(id)) {
    return [false, [{'error': 'ID has to be a number.'}]];
  } else {
    return [true, ''];
  }
}

module.exports = {validateWineWithID, validateWineWithoutID, validateID};
