/** Checks if rows are available
 * @param {any} rows - fetched rows from db query
 * @return {array} rows - returns either rows or empty array
 * */
function emptyOrRows(rows) {
  if (!rows) {
    return [];
  }
  return rows;
}

module.exports = {
  emptyOrRows,
};
