const general = {
  APIPortInternal: parseInt(process.env.API_PORT_INTERNAL),
  APIPortExternal: parseInt(process.env.API_PORT_EXTERNAL),
};

module.exports = general;
