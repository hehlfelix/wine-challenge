# Wine Challenge
## How to run the application and all tests
Requirements on your machine:  
- git
- docker
- npm 

Please follow these commands:
```bash
# Clone the repository
git clone https://gitlab.com/hehlfelix/wine-challenge.git
# Navigate to deployment/scripts directory
cd ./wine-challenge/deployment/scripts
# Make script executable
chmod +x deploy.sh
# Run script
./deploy.sh
```

After the deployment you can navigate to [APIdocs](http://localhost:8080/apidoc/apidoc.html) to explore the API.  
<br>
Also there is the rendered JSDoc documentation, reachable under [JSdocs](http://localhost:8080/jsdoc/index.html) 

## Introduction
A rising startup seeks to revolutionize the wine sector with new and disruptive ideas and
products. For a first MSP they need an awesome, flexible and scalable solution for the
management of wines. The scope for the MSP is already defined. Implement a backend
application with the following requirements.

## Functional requirements
As a wine manager I want to:
- [x] have an overview of all my wines I have in stock
- [x] know and change details about a specific wine I have in stock
- [x] add new wines to my stock
- [x] remove old wines from my stock

### Database schema
| Name        | Type             | Description                                  | Required    |
|-------------|------------------|----------------------------------------------|-------------|
| name        | string           | Name of the wine                             | true        |
| year        | number           | Production year of the wine (4 digits)       | true        |
| country     | string           | Production country of the wine               | true        |
| type        | enum <br/>string | Type of the wine (red / white / rose)        | true        |
| description | string           | Description of the wine                      | false       |
| price       | number           | Price of the wine (currency of your country) | false       |
- [x] Database container
- [x] Database schema
- [x] Database init script

## Non-functional requirements
- [x] Use the current LTS version of NodeJS to implement the application with a REST API
- [x] Use a framework of your choice to implement the application (e.g. ExpressJS)
- [x] Create automated tests to verify that your API functions correctly and provide
documentation about how to run the tests
- [x] Use a framework of your choice to implement your tests (e.g. Jest)
- [x] Create a Docker image with your application serving the REST API
- [x] Use a standard Docker image for your database of choice (e.g. MongoDB or PostgreSQL )
- [x] Provide a docker-compose file where you specify and configure
  - [x] the docker image for your application
  - [x] the docker image for your database
- [x] Provide documentation on how to run the whole application
- [x] Follow standard coding conventions (e.g. using eslint)
- [x] The application should have a stable and well-designed code base
- [x] Version your code with git and make it available on GitHub or GitLab
- [x] Document your API, e.g. by creating a swagger file

## Definition of done

The challenge is considered successful if:
- [x] The requirements of the challenge are fulfilled
- [x] Finally you briefly write
  - [x] what was particularly challenging for you during implementation
  - [x] where you personally see optimization potential within your application
  - [x] where you justify decisions such as the choice of third-party libs, architecture, etc.
