create schema api
create table api.types
(
    id   serial
        constraint types_pk
            primary key,
    name varchar not null
);

alter table api.types
    owner to postgres;

create unique index types_id_uindex
    on api.types (id);

create table api.wines
(
    id          serial
        constraint wines_pk
            primary key,
    name        varchar not null,
    year        integer not null,
    country     varchar not null,
    type        integer not null
        constraint wines_types_id_fk
            references api.types,
    description text,
    price       money
);

alter table api.wines
    owner to postgres;

create unique index wines_id_uindex
    on api.wines (id);

INSERT INTO api.types VALUES (1,'red');
INSERT INTO api.types VALUES (2,'white');
INSERT INTO api.types VALUES (3,'rose');

INSERT INTO api.wines VALUES (5, 'CAVE DU RHODAN - Petite Arvine', 2019, 'Switzerland', 2, null, '$23.50');
INSERT INTO api.wines VALUES (4, 'CAVE DU RHODAN - Humagne Rouge', 2020, 'Switzerland', 1, null, '$24.50');
INSERT INTO api.wines VALUES (2, 'Enate - Tinto Especial', 2019, 'Spain', 1, 'Fruity', '$8.99');
INSERT INTO api.wines VALUES (3, 'Rothschild - Rosé', 2020, 'France', 3, 'Lovely', '$7.99');
INSERT INTO api.wines VALUES (1, 'Boschendal - Chardonnay', 2019, 'South Africa', 2, 'Rich and tasty', '$10.99');
SELECT pg_catalog.setval('api.wines_id_seq', 6, true);