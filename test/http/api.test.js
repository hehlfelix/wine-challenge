const axios = require('axios')
const initDBContentAllWines = [
    {
        id: 5,
        name: "CAVE DU RHODAN - Petite Arvine",
        country: "Switzerland",
        type: "white",
        description: null,
        price: "$23.50"
    },
    {
        id: 4,
        name: "CAVE DU RHODAN - Humagne Rouge",
        country: "Switzerland",
        type: "red",
        description: null,
        price: "$24.50"
    },
    {
        id: 2,
        name: "Enate - Tinto Especial",
        country: "Spain",
        type: "red",
        description: "Fruity",
        price: "$8.99"
    },
    {
        id: 3,
        name: "Rothschild - Rosé",
        country: "France",
        type: "rose",
        description: "Lovely",
        price: "$7.99"
    },
    {
        id: 1,
        name: "Boschendal - Chardonnay",
        country: "South Africa",
        type: "white",
        description: "Rich and tasty",
        price: "$10.99"
    }
]
const initDBContentWine1 = {
    id: 1,
    name: "Boschendal - Chardonnay",
    country: "South Africa",
    type: "white",
    description: "Rich and tasty",
    price: "$10.99"
}
const initAllWinesAfterDelete1 = [
    {
        id: 5,
        name: "CAVE DU RHODAN - Petite Arvine",
        country: "Switzerland",
        type: "white",
        description: null,
        price: "$23.50"
    },
    {
        id: 4,
        name: "CAVE DU RHODAN - Humagne Rouge",
        country: "Switzerland",
        type: "red",
        description: null,
        price: "$24.50"
    },
    {
        id: 2,
        name: "Enate - Tinto Especial",
        country: "Spain",
        type: "red",
        description: "Fruity",
        price: "$8.99"
    },
    {
        id: 3,
        name: "Rothschild - Rosé",
        country: "France",
        type: "rose",
        description: "Lovely",
        price: "$7.99"
    }
]
const newWine = {
    name: "Van Volxem - Alte Reben",
    year: 2019,
    country: "Germany",
    type: "white",
    description: "Riesling shooting star",
    price: 18.99
}
const expectedCreatedWine = {
    country: "Germany",
    description: "Riesling shooting star",
    id: 7,
    name: "Van Volxem - Alte Reben",
    price: 18.99,
    type: "white",
    year: 2019
}

test('Get all wines after database init', () => {
    return axios
        .get('http://127.0.0.1:8080/wines/')
        .then(res => {
            return res.data
        })
        .catch(error => {
            console.error(error)
        }).then(data => {
            expect(data).toStrictEqual(initDBContentAllWines);
        });
});

test('Get wine by ID (id = 1) after database init', () => {
    return axios
        .get('http://127.0.0.1:8080/wines/1')
        .then(res => {
            return res.data
        })
        .catch(error => {
            console.error(error)
        }).then(data => {
            expect(data).toStrictEqual(initDBContentWine1);
        });
});

test('Delete wine by ID (id = 1)', () => {
    return axios
        .delete('http://127.0.0.1:8080/wines/1')
        .then(res => {
            return res.data
        })
        .catch(error => {
            console.error(error)
        }).then(() => {
            return axios
                .get('http://127.0.0.1:8080/wines/')
                .then(res => {
                    return res.data
                })
                .catch(error => {
                    console.error(error)
                });
        }).then(data => {
            expect(data).toStrictEqual(initAllWinesAfterDelete1);
        });
});

test('Create new wine', () => {
    return axios
        .post('http://127.0.0.1:8080/wines', newWine)
        .then(res => {
            return res.data
        })
        .catch(error => {
            console.error(error)
        }).then(data => {
            expect(data).toStrictEqual(expectedCreatedWine)
        });
});