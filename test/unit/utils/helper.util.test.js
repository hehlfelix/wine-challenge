
const helper = require('../../../src/utils/helper.util')

describe('Helper Utils test', () => {
  describe('emptyOrRows', () => {
    it('Should get empty array if rows is empty', () => {
      expect(helper.emptyOrRows()).toEqual([]);

    });
    it('Should get arrary if rows is filled', () => {
      expect(helper.emptyOrRows([1])).toEqual([1]);

    });    
  });
});
