const v = require("../../../src/utils/validation.util");

test('Test valid ID', () =>{
    expect(v.validateID(4)).toStrictEqual([true, ""])
})

test('Test invalid ID as String', () =>{
    expect(v.validateID("Foobar")).toStrictEqual([false, [{'error': "ID has to be a number."}]])
})

test('Checks SQL injection', () =>{
    expect(v.validateID("10%20OR%20'1'='1")).toStrictEqual([false, [{'error': "ID has to be a number."}]])
})

test('Schema with valid Wine', () =>{
    let validWine = {
        "name": "Valid wine title",
        "year": 2022,
        "country": "Validcountry",
        "type": "red",
        "description": "Testwine",
        "price": 9.99
    }
    expect(v.validateWineWithoutID(validWine)).toStrictEqual([true, ""])
})

test('Invalid datatype - year', () =>{
    let validWine = {
        "name": "Valid wine title",
        "year": "2022",
        "country": "Valid country",
        "type": "red",
        "description": "Testwine",
        "price": 9.99
    }
    expect(v.validateWineWithoutID(validWine)).toStrictEqual([false, [{
        "error": "must be number",
        "error on field": "/year",
    }]])
})

test('Invalid datatype - type', () =>{
    let validWine = {
        "name": "Valid wine title",
        "year": 2022,
        "country": "Valid country",
        "type": "sparkling",
        "description": "Testwine",
        "price": 9.99
    }
    expect(v.validateWineWithoutID(validWine)).toStrictEqual([false, [{
        "error": "must be equal to one of the allowed values red, white, rose",
        "error on field": "/type",
    }]])
})

test('Missing required param - type', () =>{
    let validWine = {
        "name": "Valid wine title",
        "year": 2022,
        "country": "Valid country",
        "description": "Testwine",
        "price": 9.99
    }
    expect(v.validateWineWithoutID(validWine)).toStrictEqual([false, [{
        "error": "must have required property 'type'",
        "error on field": "type",
    }]])
})

test('Missing multiple required param - type and year', () =>{
    let validWine = {
        "name": "Valid wine title",
        "country": "Valid country",
        "description": "Testwine",
        "price": 9.99
    }
    expect(v.validateWineWithoutID(validWine)).toStrictEqual([false, [{
        "error": "must have required property 'year'",
        "error on field": "year",
    },
        {
        "error": "must have required property 'type'",
        "error on field": "type",
    }]])
})
