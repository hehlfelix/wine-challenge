FROM node:16
WORKDIR /usr/src/winebase
COPY package*.json ./
RUN npm ci --only=production
COPY src src/
COPY docs docs/
COPY jsdoc jsdoc/
COPY index.js .
EXPOSE 8080
CMD [ "node", "index.js" ]