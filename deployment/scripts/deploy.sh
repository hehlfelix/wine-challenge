docker network create winebase-net
cd ../../
npm i
npm i redoc-cli
npm run jsdoc
npm run docs
cd deployment/
docker-compose up -d
npm run test