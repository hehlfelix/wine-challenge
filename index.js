const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const wineBaseRouter = require('./src/routes/winebase.route');
const generalConfig = require('./src/configs/general.config');
const path = require('path');
const port = generalConfig.APIPortInternal || 8080;

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
      extended: true,
    }),
);


app.disable('etag');
app.use('/apidoc', express.static(path.join(__dirname, 'docs')));
app.use('/jsdoc', express.static(path.join(__dirname, 'jsdoc')));
app.use('/wines', wineBaseRouter);

/* Error handler middleware */
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({'message': err.message});
});

app.listen(generalConfig.APIPortInternal, '0.0.0.0', () => {
  console.log(`WineBase app listening at http://localhost:${port}`);
});
